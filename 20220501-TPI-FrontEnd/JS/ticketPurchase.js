const getDomElementById = (id) => {
	return window.document.getElementById(id);

};
const studentDiscount = [80, 50, 15];
const studentType = ['Estudiante', 'Trainee', 'Junior'];
const borderType = ['primary', 'success', 'warning']

function insertStudentFee(aFeeObject, aFeeType, Adiscount, aBorderType){
    aFeeObject.innerHTML = `
        <div class="card text-center border-${aBorderType} border border-2 rounded-0" style="margin: 5px;width: 18rem;">
            <div class="card-body ">
                <h5 class="card-title" style="margin: 20px;">${aFeeType}</h5>
                <h6 class="card-subtitle mb-2 text-muted" style="margin: 20px;">Tienen un descuento</h6>
                <h5 class="card-title" style="margin: 20px;">${Adiscount}%</h5>
                <p class="card-text" style="margin: 20px;">* presentar documentación</p>

            </div>
        </div>
    `;
  
}
const studentFee = getDomElementById('student');
const traineeFee = getDomElementById('trainee');
const juniorFee = getDomElementById('junior');

const allFees = [studentFee, traineeFee, juniorFee];
for(let i = 0; i < allFees.length; i++){
    insertStudentFee(allFees[i], studentType[i], studentDiscount[i], borderType[i]);

}
const totalFee = getDomElementById('totalValue');
function insertTotalFee(){
    totalFee.innerHTML = calculateFee();

}

const ticketValue = getDomElementById('ticketValue');
const qty = getDomElementById('exampleInputQty');
const feeType = getDomElementById('feeType');

let noOptionSelected = getDomElementById('noOption');

function calculateFee(){
    const ticketValueText = ticketValue.innerText;
    let intTicketValueNumeric = parseToInt(ticketValueText);
    
    let qtyValue = qty.value;
    let qtyForCalculate;

    if(qtyValue != ""){
        qtyForCalculate = parseToInt(qtyValue);
     
    }else{
        swal("Error!", "La cantidad ingresada no puede ser vacío", "error");
        return null;

    }
    let noOptionSelectedValue = noOptionSelected.value;
    let feeTypeValue = feeType.options[feeType.selectedIndex].text;

    let feeToReturn = 0;

    if(isNaN(qtyForCalculate)){
        swal("Error!", "La cantidad ingresada no es un valor numérico", "error");
        return null;
        
    }else if(qtyForCalculate <= 0){
        swal("Error!", "La cantidad ingresada no puede ser menor o igual a 0", "error");
        return null;

    }else{
        if(feeTypeValue == noOptionSelectedValue){
            swal("Advertencia", "Elige una opción válida", "warning");

        }
        if (feeTypeValue == studentType[0]){
            feeToReturn = calculateDiscount(intTicketValueNumeric, calculatePercentage(studentDiscount[0]), qtyForCalculate);
    
        }
        if (feeTypeValue == studentType[1]){
            feeToReturn = calculateDiscount(intTicketValueNumeric, calculatePercentage(studentDiscount[1]), qtyForCalculate);
    
        }
        if (feeTypeValue == studentType[2]){
            feeToReturn = calculateDiscount(intTicketValueNumeric, calculatePercentage(studentDiscount[2]), qtyForCalculate);

        }

    }
    return feeToReturn.toString();

}
function calculatePercentage(discountType){
    let percentage = discountType / 100;
    return percentage;

}
function calculateDiscount(ticketValue, percentage, qty){
    let discount = (ticketValue - (ticketValue * percentage)) * qty;
    return discount;

}
function parseToInt(aValue){
    let valueToReturn = parseInt(aValue);
    return valueToReturn;

}
function eraseFields(){
    qty.value = '';
    totalFee.innerHTML = '';
    feeType.options[feeType.selectedIndex].text = 'Elija una opción';
    
}